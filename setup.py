#!/usr/bin/env python

import os

from setuptools import setup
from setuptools import find_packages

BASE_PATH = os.path.abspath(os.path.dirname(__file__))
REQUIREMENTS_FILE = os.path.join(BASE_PATH, 'requirements.txt')

COMMANDS = [
    'cisco_mock_cli = cisco_mock_cli.cisco_mock_cli:main',
]

PACKAGES = find_packages(BASE_PATH)

with open(REQUIREMENTS_FILE, 'rt') as requirements_file:
    REQUIREMENTS = [requirement.strip() for requirement in requirements_file.readlines()]

# Get the long description from the README file
with open(os.path.join(BASE_PATH, 'README.md'), encoding='utf-8') as readme_file:
    long_description = readme_file.read()

setup(
    name='Cisco Mock CLI',
    version='1.0',
    description='Mock CLI from cisco show tech-support log.',
    author='Micheal Taylor',
    author_email='bubthegreat@gmail.com',
    url='https://gitlab.com/bubthegreat/cisco_mock_cli.git',
    packages=PACKAGES,
    entry_points={'console_scripts': COMMANDS},
    install_requires=REQUIREMENTS,
)
